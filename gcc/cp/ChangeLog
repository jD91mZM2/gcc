2017-02-06  Jason Merrill  <jason@redhat.com>

	PR c++/71193 - incomplete types in templates
	* parser.c (cp_parser_postfix_dot_deref_expression): In a template
	handle incomplete type by pedwarning and then treating as dependent.

2017-02-06  Jakub Jelinek  <jakub@redhat.com>

	PR c++/79379
	* constexpr.c (cxx_eval_constant_expression): Handle ANNOTATE_EXPR.
	(potential_constant_expression_1): Likewise.

	PR c++/79377
	* tree.c (build_min_non_dep_op_overload): For POST{INC,DEC}REMENT_EXPR
	allow one fewer than expected arguments if flag_permissive.

	PR c++/79372
	* decl.c (cp_finish_decomp): On error set decl type to error_mark_node.
	* pt.c (tsubst_expr): Don't call tsubst_decomp_names on decompositions
	with error_mark_node type.

2017-02-03  Jason Merrill  <jason@redhat.com>

	PR c++/78689 - ICE on constructor with label
	* optimize.c (maybe_clone_body): Replace omitted parameters with
	null lvalues.
	* class.c (build_clone): Fix logic for omitting inherited parms.

	PR c++/12245 - excessive memory use
	* constexpr.c (maybe_constant_value): Fold maybe_constant_value_1
	back in.  Don't cache constants.
	(maybe_constant_init): Don't cache constants.

	PR c++/79294 - ICE with invalid template argument
	* pt.c (convert_nontype_argument_function): Check value-dependence.
	(convert_nontype_argument): Don't check it here for function ptrs.

2017-02-02  Richard Biener  <rguenther@suse.de>

	PR cp/14179
	* cp-gimplify.c (cp_fold): When folding a CONSTRUCTOR copy
	it lazily on the first changed element only and copy it
	fully upfront, only storing changed elements.

2017-02-02  Paolo Carlini  <paolo.carlini@oracle.com>

	PR c++/69637
	* decl2.c (grokbitfield): In case of error don't set-up DECL_INITIAL
	to the width.

2017-01-31  Jakub Jelinek  <jakub@redhat.com>

	PR c++/79304
	* error.c (dump_expr) <case COMPONENT_REF>: Don't print .
	after ARROW_EXPR.

2017-01-31  David Malcolm  <dmalcolm@redhat.com>

	PR c++/79298
	* name-lookup.c (suggest_alternative_in_explicit_scope): Resolve
	any namespace aliases.

2017-01-31  Nathan Sidwell  <nathan@acm.org>

	PR c++/79290
	* typeck.c (build_ptrmemfunc_access_expr): Set TREE_NO_WARNING.

	PR c++/67273
	PR c++/79253
	* pt.c: (instantiate_decl): Push to top level when current
	function scope doesn't match.  Only push lmabda scope stack when
	pushing to top.

	* cp-tree.h (instantiate_decl): Make defer_ok bool.
	* pt.c: Fix instantiate_decl calls to pass true/false not 0/1
	(instantiate_decl): Simplify and reorder state saving and restoration.

	PR c++/79264
	* lambda.c (maybe_generic_this_capture): Deal with template-id-exprs.
	* semantics.c (finish_member_declaration): Assert class is being
	defined.

2017-01-30  Alexandre Oliva <aoliva@redhat.com>

	Introduce C++ support in libcc1.
	* cp-tree.h (struct lang_identifier): Add oracle_looked_up.
	(ansi_opname): Rename to...
	(cp_operator_id): ... this.  Adjust all callers.
	(ansi_assopname): Rename to...
	(cp_assignment_operator_id): ... this.  Adjust all callers.
	(cp_literal_operator_id): Declare.
	(set_global_friend): Declare.
	(is_global_friend): Declare.
	(enum cp_oracle_request): New type.
	(cp_binding_oracle_function): New type.
	(cp_binding_oracle): Declare.
	(cp_finish_injected_record_type): Declare.
	* friend.c (global_friend): New var.
	(set_global_friend): New fn.
	(is_global_friend): New fn.
	(is_friend): Call is_global_friend.
	* name-lookup.c (cp_binding_oracle): New var.
	(query_oracle): New fn.
	(qualified_lookup_using_namespace): Call query_oracle.
	(lookup_name_real_1): Likewise.
	* parser.c (cp_literal_operator_id): Drop static.
	* search.c (friend_accessible_p): Call is_global_friend.
	* semantics.c (is_this_parameter): Accept a variable if the
	binding oracle is enabled.

2017-01-27  Jason Merrill  <jason@redhat.com>

	PR c++/78771 - ICE with inherited constructor.
	* call.c (build_over_call): Call deduce_inheriting_ctor here.
	* pt.c (tsubst_decl): Not here.
	* class.c (add_method): Or here.
	* method.c (deduce_inheriting_ctor): Handle clones.
	(implicitly_declare_fn): Don't deduce inheriting ctors yet.

2017-01-27  Adam Butcher  <adam@jessamine.co.uk>

	PR c++/64382
	* cp/parser.c (parsing_default_capturing_generic_lambda_in_template):
	New function.
	* cp/cp-tree.h: Declare it.
	* cp/semantics.c (finish_id_expression): Resolve names within a default
	capturing generic lambda defined within a template prior to
	instantiation to allow for captures to be added to the closure type.

2017-01-26  Jakub Jelinek  <jakub@redhat.com>

	PR c++/68727
	* cp-tree.def (OFFSETOF_EXPR): Bump number of operands to 2.
	* cp-tree.h (finish_offsetof): Add OBJECT_PTR argument.
	* parser.c (cp_parser_builtin_offsetof): Pass result of
	build_static_cast of null_pointer_node to finish_offsetof.
	* semantics.c (finish_offsetof): Add OBJECT_PTR argument, use
	it for -Winvalid-offsetof pedwarn instead of trying to guess
	original offsetof type from EXPR.  Save OBJECT_PTR as a new
	second operand to OFFSETOF_EXPR.
	* pt.c (tsubst_copy_and_build) <case OFFSETOF_EXPR>: Adjust
	finish_offsetof caller, pass the second operand of OFFSETOF_EXPR
	as OBJECT_PTR.

2017-01-26  Jason Merrill  <jason@redhat.com>

	* name-lookup.c (parse_using_directive): Deprecate strong using.

	PR c++/79176 - lambda ICE with -flto -Os
	* decl2.c (vague_linkage_p): Handle decloned 'tors.
	* tree.c (decl_linkage): Likewise.

2017-01-25  Martin Sebor  <msebor@redhat.com>

	* decl.c (grokdeclarator): Fix a typo in a comment.

2017-01-25  Jakub Jelinek  <jakub@redhat.com>

	PR c++/78896
	* decl.c (cp_finish_decomp): Disallow memberwise decomposition of
	lambda expressions.

	PR c++/77914
	* parser.c (cp_parser_lambda_declarator_opt): Pedwarn with
	OPT_Wpedantic on lambda templates for -std=c++14 and higher.

2017-01-25  Maxim Ostapenko  <m.ostapenko@samsung.com>

	PR lto/79061
	* decl.c (cxx_init_decl_processing): Pass main_input_filename
	to build_translation_unit_decl.

2017-01-24  Jakub Jelinek  <jakub@redhat.com>

	PR c++/79205
	* cp-gimplify.c (cp_genericize_r): Add result of
	convert_from_reference on invisiref parm to p_set.

2017-01-24  Nathan Sidwell  <nathan@acm.org>

	PR c++/78469 - defaulted ctor and inaccessible dtor
	* cp-tree.h (tsubst_flags): Add tf_no_cleanup.
	* init.c (build_new_1): Pass tf_no_cleanup to build_value_init.
	* tree.c (build_target_expr): Check tf_no_cleanup.

	PR c++/79118 - anon-members and constexpr
	* constexpr.c (cx_check_missing_mem_inits): Caller passes type not
	ctor decl.  Recursively check anonymous members.
	(register_constexpr_fundef): Adjust cx_check_missing_mem_inits
	call.
	(explain_invalid_constexpr_fn): Likewise.

2017-01-23  Nathan Sidwell  <nathan@acm.org>

	PR c++/71710 - template using directive of field
	* pt.c (tsubst_copy_and_build [COMPONENT_REF]): Move FIELD_DECL
	check earlier.

	PR c++/71406 - ICE with scope-ref'd template id exprs
	PR c++/77508
	* typeck.c (finish_class_member_access_expr): Break up SCOPE_REF
	before breaking up TEMPLATE_ID_EXPR.

2017-01-20  Nathan Sidwell  <nathan@acm.org>

	PR c++/78495 - wrong code inherited ctor and invisi-ref parm
	* cp-gimplify.c (cp_generize_r): Don't skip thunks.

2017-01-20  David Malcolm  <dmalcolm@redhat.com>

	PR c++/77829
	PR c++/78656
	* cp-tree.h (suggest_alternatives_for): Add bool param.
	(suggest_alternative_in_explicit_scope): New decl.
	* error.c (qualified_name_lookup_error): When SCOPE is a namespace
	that isn't the global one, call new function
	suggest_alternative_in_explicit_scope, only calling
	suggest_alternatives_for if it fails, and disabling near match
	searches fort that case.  When SCOPE is the global namespace,
	pass true for new param to suggest_alternatives_for to allow for
	fuzzy name lookups.
	* lex.c (unqualified_name_lookup_error): Pass true for new param
	to suggest_alternatives_for.
	* name-lookup.c (consider_binding_level): Add forward decl.
	(suggest_alternatives_for): Add "suggest_misspellings" param,
	using it to conditionalize the fuzzy name-lookup code.
	(suggest_alternative_in_explicit_scope): New function.
	* parser.c (cp_parser_primary_expression): When calling
	finish_id_expression, pass location of id_expression rather
	than that of id_expr_token.
	(cp_parser_id_expression): Convert local "unqualified_id" from
	tree to cp_expr to avoid implicitly dropping location information.

2017-01-20  Marek Polacek  <polacek@redhat.com>

	PR c/64279
	* call.c (build_conditional_expr_1): Warn about duplicated branches.
	* semantics.c (finish_expr_stmt): Build statement using the proper
	location.

2017-01-19  Jason Merrill  <jason@redhat.com>

	US 20 - forwarding references and class template argument deduction
	* cp-tree.h (TEMPLATE_TYPE_PARM_FOR_CLASS): New.
	* pt.c (push_template_decl_real): Set it.
	(maybe_adjust_types_for_deduction): Check it.
	(rewrite_template_parm): Copy it.

	US 19 - deduction guides and constructors
	* call.c (joust): Prefer deduction guides to constructors.
	* pt.c (build_deduction_guide): Set DECL_ARTIFICIAL.
	(deduction_guide_p): Check DECL_P.

	* decl.c (check_initializer): Always use build_aggr_init for array
	decomposition.

	PR c++/79130 - decomposition and direct-initialization
	* init.c (build_aggr_init): Communicate direct-initialization to
	build_vec_init.
	(build_vec_init): Check for array copy sooner.
	* parser.c (cp_parser_decomposition_declaration): Remove call to
	build_x_compound_expr_from_list.

2017-01-18  Jason Merrill  <jason@redhat.com>

	PR c++/68666 - member variable template-id
	* typeck.c (finish_class_member_access_expr): Handle variable
	template-id.
	* pt.c (lookup_and_finish_template_variable): No longer static.
	* cp-tree.h: Declare it.

2017-01-18  Nathan Sidwell  <nathan@acm.org>

	PR c++/78488
	* call.c (build_over_call): When checking ellipsis conversions for
	an inherited ctor, make sure there is at least one conversion.

2017-01-18  Jason Merrill  <jason@redhat.com>

	PR c++/78894 - ICE with class deduction and default arg
	* pt.c (build_deduction_guide): Set DECL_PRIMARY_TEMPLATE.

2017-01-18  Markus Trippelsdorf  <markus@trippelsdorf.de>

	PR c++/77489
	* mangle.c (write_discriminator): Reorganize abi warning check.

2017-01-18  Nathan Sidwell  <nathan@acm.org>

	* cp-tree.h: Clarify exception spec node comment.
	* except.c (nothrow_spec_p): Simplify by checking node-equality.

	PR c++/79091
	* mangle.c (write_exception_spec): Check nothrow explicitly.
	(write_encoding): Don't increment processing_template_decl around
	encoding.

2017-01-18  Markus Trippelsdorf  <markus@trippelsdorf.de>

	PR c++/70182
	* mangle.c (write_template_args): Add "on" for operator names.

2017-01-18  Markus Trippelsdorf  <markus@trippelsdorf.de>

	PR c++/77489
	* mangle.c (write_discriminator): Handle discriminator >= 10.

2017-01-17  Nathan Sidwell  <nathan@acm.org>

	PR c++/61636
	* cp-tree.h (maybe_generic_this_capture): Declare.
	* lambda.c (resolvable_dummy_lambda): New, broken out of ...
	(maybe_resolve_dummy): ... here.  Call it.
	(maybe_generic_this_capture): New.
	* parser.c (cp_parser_postfix_expression): Speculatively capture
	this in generic lambda in unresolved member function call.
	* pt.c (tsubst_copy_and_build): Force hard error from failed
	member function lookup in generic lambda.

2017-01-17  Aldy Hernandez  <aldyh@redhat.com>

	PR c++/70565
	* cp-array-notation.c (expand_array_notation_exprs): Handle
	OMP_PARALLEL.

2017-01-11  Jason Merrill  <jason@redhat.com>

	PR c++/78337 - ICE on invalid with generic lambda
	* semantics.c (process_outer_var_ref): Check if containing_function
	is null.  Move inform call under complain test.

2017-01-11  Nathan Sidwell  <nathan@acm.org>

	PR c++/77812
	* name-lookup.c (set_namespace_binding_1): An overload of 1 decl
	is a new overload.

2017-01-11  Nathan Sidwell  <nathan@acm.org>

	* name-lookup.c (push_overloaded_decl_1): Refactor OVERLOAD creation.

2017-01-11  Jakub Jelinek  <jakub@redhat.com>

	PR c++/78341
	* parser.c (cp_parser_std_attribute_spec): Remove over-eager
	assertion.  Formatting fix.

	PR c++/72813
	* decl2.c (c_parse_final_cleanups): Set flag_syntax_only to 1 after
	writing PCH file.

2017-01-10  David Malcolm  <dmalcolm@redhat.com>

	PR c++/77949
	* parser.c (cp_parser_class_specifier_1): Only suggest inserting
	a missing semicolon if we have a valid insertion location for
	the fix-it hint.

2017-01-10  Jason Merrill  <jason@redhat.com>

	FI 20, decomposition declaration with parenthesized initializer.
	* parser.c (cp_parser_decomposition_declaration): Use
	cp_parser_initializer.

2017-01-09  Jason Merrill  <jason@redhat.com>

	Implement P0195R2, C++17 variadic using.
	* parser.c (cp_parser_using_declaration): Handle ellipsis and comma.
	* pt.c (tsubst_decl): Handle pack expansion in USING_DECL_SCOPE.
	* error.c (dump_decl): Likewise.

2017-01-09  Jakub Jelinek  <jakub@redhat.com>

	PR translation/79019
	PR translation/79020
	* semantics.c (finish_omp_clauses): Add missing whitespace to
	translatable strings.
	* cp-cilkplus.c (cpp_validate_cilk_plus_loop_aux): Fix comment typo.

2017-01-07  Jason Merrill  <jason@redhat.com>

	PR c++/78948 - instantiation from discarded statement
	* parser.h (struct cp_parser): Remove in_discarded_stmt field.
	* cp-tree.h (in_discarded_stmt): Declare it.
	(struct saved_scope): Add discarded_stmt bitfield.
	(in_discarded_stmt): New macro.
	* decl2.c (mark_used): Check it.
	* parser.c (cp_parser_selection_statement): Adjust.
	(cp_parser_jump_statement): Adjust.

2017-01-05  Jakub Jelinek  <jakub@redhat.com>

	PR c++/78931
	* decl.c (cp_finish_decomp): Remove probe variable, if tt is
	REFERENCE_REF_P, set tt to its operand.

	PR c++/78890
	* class.c (check_field_decls): Diagnose REFERENCE_TYPE fields in
	unions even for C++11 and later.

2017-01-05  Nathan Sidwell  <nathan@acm.org>

	PR c++/78765
	* pt.c (convert_nontype_argument): Don't try and see if integral
	or enum expressions are constants prematurely.

2017-01-04  Marek Polacek  <polacek@redhat.com>

	PR c++/64767
	* typeck.c (cp_build_binary_op): Warn when a pointer is compared with
	a zero character literal.

2017-01-04  Jakub Jelinek  <jakub@redhat.com>

	PR c++/78949
	* typeck.c (cp_build_unary_op): Call mark_rvalue_use on arg if it has
	vector type.

	PR c++/78693
	* parser.c (cp_parser_simple_declaration): Only complain about
	inconsistent auto deduction if auto_result doesn't use auto.

	* parser.c (cp_parser_simple_declaration): Diagnose function
	declaration among more than one init-declarators with auto
	specifier.

	PR c++/71182
	* parser.c (cp_lexer_previous_token): Use vec_safe_address in the
	assertion, as lexer->buffer may be NULL.

2017-01-04  Marek Polacek  <polacek@redhat.com>

	PR c++/77545
	PR c++/77284
	* constexpr.c (potential_constant_expression_1): Handle CLEANUP_STMT.

2017-01-04  Nathan Sidwell  <nathan@acm.org>

	PR c++/66735
	* cp-tree.h (DECLTYPE_FOR_REF_CAPTURE): New.
	(lambda_capture_field_type): Update prototype.
	* lambda.c (lambda_capture_field_type): Add is_reference parm.
	Add referenceness here.
	(add_capture): Adjust lambda_capture_field_type call, refactor
	error checking.
	* pt.c (tsubst): Adjust lambda_capture_field_type call.

2017-01-01  Jakub Jelinek  <jakub@redhat.com>

	Update copyright years.

Copyright (C) 2017 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.
